package org.gpf.fishlord;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;

class Fish implements Runnable {
	
	private static final int BASE_STEP = 5;				// x和y坐标的步进值的参考标准
	int speedOfFish = 20;								// 控制鱼的速度
	
	int x, y, index, width, height, xStep,yStep;

	BufferedImage fishImage;                          	// 当前鱼的背景图
	BufferedImage[] fishImages = new BufferedImage[10];	// 一条鱼的所有帧的图片
	Random r;											// 产生随机数

	/**
	 * 在构造器中初始化鱼的属性
	 * @param fishname
	 * @throws IOException
	 */
	public Fish(String fishName) throws IOException {
		// 加载鱼的图片
		BufferedImage tempFishImage;
		String resourceName;
		for (int i = 0; i < 10; i++) {
			if (i!=9) {
				resourceName = "/images/" + fishName + "_0" + (i+1) + ".png";
			}else {
				resourceName = "/images/" + fishName + "_" + (i+1) + ".png";
			}
			tempFishImage = ImageIO.read(getClass().getResourceAsStream(resourceName));
			fishImages[i] = tempFishImage;
		}
		fishImage = fishImages[index];
		
		width = fishImage.getWidth();   					// 根据背景图片的宽高设置鱼所占矩形区域的大小
		height = fishImage.getHeight();
		
		goInPool();
	}

	/**
	 * 维持鱼的游动---x和y坐标的变化
	 */
	public void run() {
		while (true) {
			try {
				Thread.sleep(speedOfFish); // 如果不休眠，鱼的速度过快，基本感觉不到鱼的存在。视觉暂留：1/24~1/7秒之间
				index++;
				fishImage = fishImages[index % fishImages.length]; // 轮流切换帧，生成动画
				x = x - xStep;
				int temp = r.nextInt(10) + 1;
				yStep = r.nextBoolean()?temp:-temp;
//				y = y + yStep;
				
				// 判断鱼是否到了边界，因为鱼是从右面进入的，因此只需要判断3个方向
				if (x <= 0 || y <= 0 || y >= 480){
					goInPool();
				}
				
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 *  判断鱼是否在网内
	 */
	public boolean fishInNet(int netX, int netY) {
		int dx = netX - x;
		int dy = netY - y;
		return dx >= 0 && dx <= width && dy >= 0 && dy <= height;
	}

	/**
	 * 鱼从屏幕上游出或者被网罩住的时候消失，再从屏幕的右侧游动到屏幕中,实际上还是那几条鱼
	 */
	void goInPool() {
		r = new Random();
		x = FishlordFrame.WIDTH - 10;						// 鱼从右侧游动到屏幕左侧
		y = r.nextInt(FishlordFrame.HEIGHT - 20 - height);	// 鱼的初始y的坐标是根据窗体的高度随机指定的
		xStep = r.nextInt(BASE_STEP) + 1;					// 鱼游动的速度是随机的                        
	}
}