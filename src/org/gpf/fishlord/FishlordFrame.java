package org.gpf.fishlord;

import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * 游戏窗体
 * @author gaopengfei
 * @date 2015-4-12 下午7:39:19
 */
public class FishlordFrame extends JFrame {

	public static final int WIDTH = 800;
	public static final int HEIGHT = 480;
	
	private Pool pool;
	
	/**
	 * 构造器中初始化界面
	 */
	public FishlordFrame() {
		
		this.setTitle("捕鱼达人");
		this.setSize(WIDTH, HEIGHT);
		this.setLocationRelativeTo(null); // 设置窗口居中，必须放在setSize之后
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		try {
			pool = new Pool();
			this.getContentPane().add(pool);
			this.setVisible(true);
			pool.action();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(this, "加载资源失败！","应用程序错误",JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, "初始化游戏失败！","应用程序错误",JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
		
	}
	
}
